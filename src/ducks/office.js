import firebase from 'firebase';
import {appName} from '../config';
import {eventChannel, delay} from 'redux-saga';
import {put, call, takeEvery, all, fork, spawn, cancel, take} from 'redux-saga/effects';
import {fbDatatoList} from './utils';
import {createSelector} from 'reselect';
import {reset} from 'redux-form';
import {Record, OrderedMap} from 'immutable';
import {NAME_FORM} from '../components/Offices/NewOffice';

export const ReducerState = Record({
	list: new OrderedMap({}),
	error: null,
	loading: false,
	editable: ''
});

const OfficeRecord = Record({
	uid: null,
	country: null,
	province: null,
	postal_code: null,
	city: null,
	street: null,
	type: null,
	address2: null,
	phone: null,
	fax: null,
	email: null
});

export const moduleName = 'office';
const prefix = `${appName}/${moduleName}`;
export const FETCH_ALL_REQUEST = `${prefix}/FETCH_ALL_REQUEST`;
export const FETCH_ALL_SUCCESS = `${prefix}/FETCH_ALL_SUCCESS`;
export const ADD_OFFICE_REQUEST = `${prefix}/ADD_OFFICE_REQUEST`;
export const ADD_OFFICE_SUCCESS = `${prefix}/ADD_OFFICE_SUCCESS`;
export const ADD_OFFICE_ERROR = `${prefix}/ADD_OFFICE_ERROR`;
export const EDIT_OFFICE_OPEN = `${prefix}/EDIT_OFFICE_OPEN`;
export const EDIT_OFFICE_START = `${prefix}/EDIT_OFFICE_START`;
export const EDIT_OFFICE_CLOSE = `${prefix}/EDIT_OFFICE_CLOSE`;
export const EDIT_OFFICE_REQUEST = `${prefix}/EDIT_OFFICE_REQUEST`;
export const EDIT_OFFICE_SUCCESS = `${prefix}/EDIT_OFFICE_SUCCESS`;
export const EDIT_OFFICE_ERROR = `${prefix}/EDIT_OFFICE_ERROR`;
export const DELETE_OFFICE_REQUEST = `${prefix}/DELETE_OFFICE_REQUEST`;
export const DELETE_OFFICE_SUCCESS = `${prefix}/DELETE_OFFICE_SUCCESS`;
export const DELETE_OFFICE_ERROR = `${prefix}/DELETE_OFFICE_ERROR`;

export default function reducer(state = new ReducerState(), action) {
	const {type, payload} = action;

	switch (type) {
		case FETCH_ALL_REQUEST:
		case ADD_OFFICE_REQUEST:
		case EDIT_OFFICE_REQUEST:
		case DELETE_OFFICE_REQUEST:
			return state.set('loading', true);

		case ADD_OFFICE_SUCCESS:
			return state
				.set('loading', false)
				.setIn(['list', payload.uid], new OfficeRecord(payload));

		case EDIT_OFFICE_START:
			return state.set('editable', payload);

		case EDIT_OFFICE_SUCCESS:
			return state
				.set('loading', false)
				.updateIn(['list', payload.uid], ()=>new OfficeRecord({...payload.office, uid: payload.uid}));

		case EDIT_OFFICE_CLOSE:
			return state.set('editable', null);

		case DELETE_OFFICE_SUCCESS:
			return state
				.set('loading', false)
				.deleteIn(['list', payload]);

		case FETCH_ALL_SUCCESS:
			return state
				.set('loading', false)
				.set('list', fbDatatoList(payload, OfficeRecord));

		default:
			return state;
	}
}

export const stateSelector = state => state[moduleName];
export const listSelector = createSelector(stateSelector, state => state.list);
export const officeListSelector = createSelector(listSelector, list => list.valueSeq().toArray());

export function addOffice(office) {
	return {
		type: ADD_OFFICE_REQUEST,
		payload: office
	}
}

export function editOffice(uid: string, office) {
	return {
		type: EDIT_OFFICE_REQUEST,
		payload: {uid, office}
	}
}

export function editOfficeOpen(payload) {
	return {
		type: EDIT_OFFICE_OPEN,
		payload
	}
}

export function editOfficeClose() {
	return {
		type: EDIT_OFFICE_CLOSE
	}
}

export function deleteOffice(uid) {
	return {
		type: DELETE_OFFICE_REQUEST,
		payload: uid
	}
}

export const addOfficeSaga = function * (action) {
	const officeRef = firebase.database().ref('office');

	try {
		const ref = yield call([officeRef, officeRef.push], action.payload);

		yield put({
			type: ADD_OFFICE_SUCCESS,
			payload: {...action.payload, uid: ref.key}
		});

		yield put(reset(NAME_FORM));
	} catch (error) {
		yield put({
			type: ADD_OFFICE_ERROR,
			error
		});
	}
}

export const deleteOfficeSaga = function * (action) {
	const {payload} = action;
	const officeRef = firebase.database().ref('office/'+payload);

	try {
		yield call([officeRef, officeRef.remove]);

		yield put({
			type: DELETE_OFFICE_SUCCESS,
			payload
		});
	} catch (error) {
		yield put({
			type: DELETE_OFFICE_ERROR,
			error
		});
	}
}

export const editOfficeOpenSaga = function * (action) {
	const {payload} = action;
	if (!payload.editable) {
		yield put({
			type: EDIT_OFFICE_START,
			payload: payload.uid
		});
	}
	else {
		yield put({
			type: EDIT_OFFICE_CLOSE
		});
		yield call(delay, 100);
		yield put({
			type: EDIT_OFFICE_START,
			payload: payload.uid
		});
	}
}

export const editOfficeSaga = function * (action) {
	const {payload, payload: {uid, office}} = action;
	const officeRef = firebase.database().ref('office/'+uid);

	try {
		yield call([officeRef, officeRef.update], office);
		yield put({
			type: EDIT_OFFICE_SUCCESS,
			payload
		});
		yield put({type: EDIT_OFFICE_CLOSE});
	} catch (error) {
		yield put({
			type: EDIT_OFFICE_ERROR,
			error
		});
	}
}

export const cancellableSync = function * () {
	yield put({type: FETCH_ALL_REQUEST});

	let task;
	while (true) {
		const {payload} = yield take('@@router/LOCATION_CHANGE');

		if (payload && payload.pathname.includes('contact/office')) {
			task = yield fork(realtimeSync);
		} else if (task) {
			yield cancel(task);
		}
	}
}

export const realtimeSync = function * () {
	const chan = yield call(createOfficeSocket);
	try {
		while (true) {
			const {data} = yield take(chan);

			yield put({
				type: FETCH_ALL_SUCCESS,
				payload: data.val()
			});
		}
	} finally {
		yield call([chan, chan.close]);
	}
}

const createOfficeSocket = () => eventChannel(emmit => {
	const ref = firebase.database().ref('office');
	const callback = (data) => emmit({ data });
	ref.on('value', callback);
	return () => {
		ref.off('value', callback)
	}
});

export const saga = function * () {
	yield spawn(cancellableSync);

	yield all([
		takeEvery(ADD_OFFICE_REQUEST, addOfficeSaga),
		takeEvery(EDIT_OFFICE_REQUEST, editOfficeSaga),
		takeEvery(EDIT_OFFICE_OPEN, editOfficeOpenSaga),
		takeEvery(DELETE_OFFICE_REQUEST, deleteOfficeSaga)
	]);
}