// @flow

import {combineReducers} from 'redux'
import {routerReducer as router} from 'react-router-redux'
import {reducer as form} from 'redux-form'
import officeReducer, {moduleName as officeModule} from '../ducks/office'

export default combineReducers({
    router, form,
    [officeModule]: officeReducer,
})