import {saga as officeSaga} from '../ducks/office'
import {all} from 'redux-saga/effects'

export default function * rootSaga() {
    yield all([
        officeSaga()
    ])
}