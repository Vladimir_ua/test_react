// @flow

import firebase from 'firebase';

export const appName = "test-react-3bc76";
export const firebaseConfig = {
    apiKey: "AIzaSyC-9YuY0AjOPj3xg1hQwFPPiuyE2gftn2s",
    authDomain: `${appName}.firebaseapp.com`,
    databaseURL: `https://${appName}.firebaseio.com`,
    projectId: appName,
    storageBucket: `${appName}.appspot.com`,
    messagingSenderId: "837649797088"
};

firebase.initializeApp(firebaseConfig);