// @flow

import React, { Component } from 'react';
import Autocomplete from 'react-autosuggest';
import './style.css';

const getSuggestions = (value, list) => {
	const inputValue = value.trim().toLowerCase();
	const inputLength = inputValue.length;

	return inputLength === 0 ? list : list.filter(name =>
		name.toLowerCase().slice(0, inputLength) === inputValue
	);
};

const getSuggestionValue = suggestion => suggestion;

const renderSuggestion = suggestion => (
	<div className="element">
		{suggestion}
	</div>
);

type Props = {
	options: Array<Object>,
	input: Object,
	initialValue?: string,
	className?: string,
	placeholder?: string,
	onChangeValue: Function
};

type State = {
	value: string,
	suggestions: Array<Object>
};

class Autosuggest extends Component<Props, State> {
	constructor(props: any) {
		super(props);

		this.state = {
			value: this.props.initialValue||'',
			suggestions: this.props.options
		}
	}

	componentWillReceiveProps(nextProps: Object) {
		if (nextProps.input && nextProps.input.value === '' && this.state.value !== '') {
			this.setState({
				value: nextProps.input.value
			});
		}
	}

	onChange = (event: string, obj: Object) => {
		if (this.props.onChangeValue) {
			this.props.onChangeValue(obj.newValue);
		}
		this.setState({
			value: obj.newValue
		});
		this.props.input.onChange(event);
	}

	onSuggestionsFetchRequested = (input: Object) => {
		this.setState({
			suggestions: getSuggestions(input.value, this.props.options)
		});
	}

	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	}

	render() {
		const {input, className} = this.props;
		const {value, suggestions} = this.state;

		const inputProps = {
			...input,
			className,
			value,
			onChange: this.onChange
		};
		return (
			<Autocomplete
				suggestions={suggestions}
				onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
				onSuggestionsClearRequested={this.onSuggestionsClearRequested}
				getSuggestionValue={getSuggestionValue}
				renderSuggestion={renderSuggestion}
				inputProps={inputProps}
				shouldRenderSuggestions={()=>true}
			/>
		);
	}
}

export default Autosuggest;