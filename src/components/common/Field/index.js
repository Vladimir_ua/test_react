// @flow

import React, { Component } from 'react';
import Autosuggest from './Autosuggest';

type Props = {
	attr: Object,
	input: Object,
	nameField: string,
	label: string,
	initialValue: string,
	type: string,
	classWrp: string,
	htmlFor: string,
	classLabel: string,
	className: string,
	meta: Object,
	options?: Array<Object>,
	onChangeValue: Function
};

class ErrorField extends Component<Props> {
	constructor(props: any) {
		super(props);

		const {initialValue, type, input} = this.props;
		if (initialValue && initialValue !== '') {
			if (type === 'checkbox') {
				input.onChange(initialValue ? 1 : 0);
			}
			else {
				input.onChange(initialValue);
			}
			input.onBlur();
		}
	}

	render() {
		const {nameField, label, attr, input, initialValue, type,
			classWrp = '', classLabel = '', htmlFor, className,
			meta: {error, touched}
		} = this.props;
		const errorText = touched && error;
		const classElement = (className||"form-control")+(errorText?" is-invalid":"");
		let element;

		switch (type) {
			case 'autosuggest':
				element = (
					<Autosuggest
						onChangeValue={this.props.onChangeValue}
						className={classElement}
						initialValue={initialValue}
						input={{...input, ...attr}}
						options={this.props.options||[]}
					/>
				);
				break;
			case 'textarea':
				element = <textarea className={classElement} {...input} {...attr}>{initialValue}</textarea>;
				break;
			default: element = <input className={classElement} id={htmlFor} {...input} {...attr} type={type} />;
		}

		if (nameField) {
			return (
				<div className="form-group row" title={errorText||''}>
					<label className="col-form-label">{nameField}</label>
					<div className={classWrp}>
						{element}
						<label className={classLabel} htmlFor={htmlFor}>{label}</label>
					</div>
				</div>
			);
		}
		return (
			<div className={classWrp.concat(' form-group row')} title={errorText||''}>
				{label && <label className={classLabel.concat(' col-form-label')} htmlFor={htmlFor}>{label}</label>}
				{element}
			</div>
		);
	}
}

export default ErrorField;