// @flow

import React, {Component} from 'react';
import {withRouter} from 'react-router'
import {NavLink} from 'react-router-dom';
import './style.css';
import {Collapse, NavItem, Nav} from 'reactstrap';
import layer from './img/layer.png';
import layer1_5 from './img/layer@1.5x.png';

type Props = {
	location: Object
};

type State = {
	isOpen: boolean
};

class Menu extends Component<Props, State> {
	state = {
		isOpen: false
	}

	isActiveNavLink = (path: string) => {
		return this.props.location.pathname.startsWith(path);
	}

	getNavLinkClass = (path: string) => {
		return this.isActiveNavLink(path) ? 'active ' : '';
	}

	render() {
		return (
			<nav className="side-navbar">
				<div className="side-navbar-head">
					<img src={layer} srcSet={layer1_5+" 1.5x"} className="layer" alt="layer" />
				</div>
				<Nav className="nav flex-column" navbar>
					<NavItem className={this.getNavLinkClass("/contact")}>
						<NavLink exact to="/contact" className="nav-link">COMPANY INFO</NavLink>
						<Collapse isOpen={this.isActiveNavLink("/contact")} navbar>
							<Nav className="ml-auto" navbar>
								<NavItem className={this.getNavLinkClass("/contact/info")}>
									<NavLink to="/contact/info" className="nav-link">Basic Info</NavLink>
								</NavItem>
								<NavItem className={this.getNavLinkClass("/contact/office")}>
									<NavLink to="/contact/office" className="nav-link">Offices</NavLink>
								</NavItem>
								<NavItem className={this.getNavLinkClass("/contact/competitors")}>
									<NavLink to="/contact/competitors" className="nav-link">Competitors</NavLink>
								</NavItem>
							</Nav>
						</Collapse>
					</NavItem>
					<NavItem className="nav-item">
						<NavLink to="/firm" className="nav-link">MY FIRM</NavLink>
					</NavItem>
					<NavItem className="nav-item">
						<NavLink to="/deals" className="nav-link">DEALS</NavLink>
					</NavItem>
					<NavItem className="nav-item">
						<NavLink to="/financials" className="nav-link">FINANCIALS</NavLink>
					</NavItem>
				</Nav>
			</nav>
		);
	}
}

export default withRouter(Menu);

