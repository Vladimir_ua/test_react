// @flow

import React, {Component} from 'react';
import './style.css';

type Props = {
    parentName: string,
    childrenName?: string
};

class HeaderPage extends Component<Props> {
    render() {
        const {childrenName, parentName} = this.props;
        const useMessage = true;
        return (
            <div className="header-page">
                {childrenName ? <span className="children-name">{childrenName}</span>: ''}
                <span className="parent-name">{parentName}</span>
                {useMessage ? <div>Updating your location and contact information helps you appeal to regional investors and service providers.</div>:""}
                <hr />
            </div>
        )
    }
}

export default HeaderPage;