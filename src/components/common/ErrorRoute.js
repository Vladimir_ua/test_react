// @flow

import React from 'react'
import HeaderPage from './HeaderPage';

function ErrorRoute() {
    return (
        <HeaderPage parentName="Undefined route" />
    )
}

export default ErrorRoute