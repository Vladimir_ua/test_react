// @flow

import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Collapse, Navbar, NavbarToggler, NavItem, Nav} from 'reactstrap';
import './style.css';
import logo from './svg/spd-u.svg';

type State = {
	isOpen: boolean
};

class Head extends Component<{}, State> {
	state = {
		isOpen: false
	}

	toggle = () => {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	render() {
		return (
			<Navbar className="header" dark expand="md">
				<Link to="/" className="link-spd navbar-brand">
					<img src={logo} className="spd-u" alt="spd" />
				</Link>
				<NavbarToggler onClick={this.toggle} />
				<Collapse isOpen={this.state.isOpen} navbar>
					<Link to="/" className="nav-link link-profile-editor">Profile Editor</Link>
					<Nav className="ml-auto" navbar>
						<NavItem>
							<Link to="/" className="nav-link">Contact</Link>
						</NavItem>
						<NavItem>
							<Link to="/" className="nav-link">FAQs</Link>
						</NavItem>
						<NavItem>
							<Link to="/" className="nav-link">Save and Exit</Link>
						</NavItem>
					</Nav>
				</Collapse>
			</Navbar>
		);
	}
}

export default Head;

