// @flow

import React, {Component} from 'react';
import HeaderPage from '../common/HeaderPage';

class StartPage extends Component<{}> {
    render() {
        return (
            <HeaderPage parentName="start page" />
        )
    }
}

export default StartPage;