// @flow

import React, {Component} from 'react';
import HeaderPage from '../../common/HeaderPage';

class CompetitorsPage extends Component<{}> {
    render() {
        return (
            <HeaderPage parentName="company info" childrenName="competitors" />
        )
    }
}

export default CompetitorsPage;