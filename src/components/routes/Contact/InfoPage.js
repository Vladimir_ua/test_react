// @flow

import React, {Component} from 'react';
import HeaderPage from '../../common/HeaderPage';

class InfoPage extends Component<{}> {
    render() {
        return (
            <HeaderPage parentName="company info" childrenName="basic info" />
        )
    }
}

export default InfoPage;