// @flow

import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import HeaderPage from '../../common/HeaderPage';
import InfoPage from './InfoPage';
import OfficesPage from './OfficesPage/index';
import CompetitorsPage from './CompetitorsPage';
import ErrorRoute from '../../common/ErrorRoute';

class ContactPage extends Component<{}> {

    contactRoute() {
        return (
            <HeaderPage parentName="company info" />
        );
    }

    render() {
        return (
            <Switch>
                <Route exact path="/contact" component={this.contactRoute} />
                <Route path="/contact/info" component={InfoPage} />
                <Route path="/contact/office" component={OfficesPage} />
                <Route path="/contact/competitors" component={CompetitorsPage} />
                <Route component={ErrorRoute} />
            </Switch>
        )
    }
}

export default ContactPage;
