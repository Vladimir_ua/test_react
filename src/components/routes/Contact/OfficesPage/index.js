// @flow

import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import HeaderPage from '../../../common/HeaderPage';
import OfficesEditor from '../../../Offices/OfficesEditor';
import './style.css';

class OfficesPage extends Component<{}> {
    render() {
        return (
            <div className="offices-page">
                <HeaderPage parentName="company info" childrenName="offices" />
                <div className="offices-content">
                    <OfficesEditor />
                    <hr />
                    <div className="footer">
                        <div className="d-inline-block">
                            <button type="button" className="btn btn-outline-dark">Back</button>
                            <Link to="/" className="nav-link d-inline-block">
                                <i className="fa fa-plus-circle"></i>
                                Provide additional comments
                            </Link>
                        </div>
                        <div className="float-right">
                            <button type="button" className="btn btn-outline-dark">Skip this step</button>
                            <button type="button" className="btn btn-primary">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default OfficesPage;