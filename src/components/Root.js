// @flow

import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import Head from './common/Head';
import Menu from './common/Menu';
import StartPage from './routes/StartPage';
import ContactPage from './routes/Contact';
import ErrorRoute from './common/ErrorRoute';
import './Root.css'

class Root extends Component<{}> {
    render() {
        return (<div>
            <Head/>
            <div className="container-fluid">
                <div className="page-content d-flex">
                    <Menu/>
                    <div className="content-inner">
                        <Switch>
                            <Route exact path="/" component={StartPage}/>
                            <Route path="/contact" component={ContactPage}/>
                            <Route component={ErrorRoute} />
                        </Switch>
                    </div>
                </div>
            </div>
        </div>)
    }
}

export default Root;
