// @flow

import React, {Component} from 'react';
import {connect} from 'react-redux';
import OfficeAdd from '../OfficeAdd';
import OfficesList from '../OfficesList';
import {officeListSelector} from '../../../ducks/office'
import './style.css';

type Props = {
	list: Object
};

class OfficesEditor extends Component<Props> {
	render() {
		return (
			<div className="offices-editor">
				<OfficeAdd />
				<OfficesList list={this.props.list} />
			</div>
		)
	}
}

export default connect(state => ({
	list: officeListSelector(state)
}))(OfficesEditor);