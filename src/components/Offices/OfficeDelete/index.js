// @flow

import React, { Component } from 'react';
import {reduxForm, Field} from 'redux-form';
import ErrorField from '../../common/Field/index';
import './style.css';

type Props = {
	onCancel: Function,
	handleSubmit: Function
};

type State = {
	country: string
}

const listType = [
	'Former Record',
	'Duplicate Record',
	'Record never Existed',
	'Other'
];

export class OfficeDelete extends Component<Props, State> {
	changeCountry = (value: string) => {
		this.setState({
			country: value
		});
	}

	render() {
		const {handleSubmit, onCancel} = this.props;

		return (
			<form onSubmit={handleSubmit} className="office-delete">
				<div className="title">Please tell us why you’re removing this record.</div>
				<Field label={false}
				       name="type"
				       classWrp="text-capitalize autocomplete"
				       type="autosuggest"
				       component={ErrorField}
				       attr={{placeholder:"Former Record"}}
				       options={listType}
				       onChangeValue={this.changeCountry}
				/>
				<Field label="Notes:"
				       name="notes"
				       type="textarea"
				       classWrp="textarea-wrp"
				       component={ErrorField}/>
				<div className="row-btn">
					<button type="button" className="btn btn-outline-dark" onClick={onCancel}>
						Cancel
					</button>
					<button type="submit" className="btn btn-primary float-right">
						Remove Record
					</button>
				</div>
			</form>
		)
	}
}

export const NAME_FORM = 'office_delete';

export default reduxForm({
	form: NAME_FORM
})(OfficeDelete);