// @flow

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {addOffice} from '../../../ducks/office';
import OfficesAmount from '../OfficesAmount';
import NewOffice from '../NewOffice/index';
import './style.css';

type Props = {
	addOffice: Function
};

type State = {
	isOpen: boolean
};

class OfficeAdd extends Component<Props, State> {
	state = {
		isOpen: false
	}

	toggle = () =>  {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	render() {
		return (
			<div className="office-add">
				<button type="button" className="btn btn-outline-dark" onClick={this.toggle}>Add New Office</button>
				<OfficesAmount addClass="float-right" addStyle={{marginTop:'9px'}} />
				{this.state.isOpen ? <NewOffice onSubmit={this.props.addOffice} /> : ''}
			</div>
		);
	}
}

export default connect(null, {addOffice})(OfficeAdd);