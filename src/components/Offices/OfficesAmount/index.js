// @flow

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {moduleName} from '../../../ducks/office';
import './style.css';

type Props = {
	loading: boolean,
	size: number,
	addClass?: string,
	addStyle?: Object
};

class OfficesAmount extends Component<Props> {
	render() {
		const {loading, addClass, addStyle, size} = this.props;
		return (
			<div className={(addClass ? addClass+' ': '')+'offices-amount'} style={addStyle}>
				{loading ? <b>loading...</b> : size+(size === 1 ? ' Office':' Offices')}
			</div>
		);
	}
}

export default connect(state => ({
	loading: state[moduleName].loading,
	size: state[moduleName].list.size
}))(OfficesAmount);
