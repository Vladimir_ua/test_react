// @flow

import React, { Component } from 'react';
import {reduxForm, Field} from 'redux-form';
import validateEmail from 'email-validator';
import ErrorField from '../../common/Field/index';
import './style.css';

type Props = {
	handleSubmit: Function,
	pristine: boolean,
	submitting: boolean,
	reset: Function,
	office: Object,
	onCancel: Function
};

type State = {
	country: string
}

const listCountry = [
	'Ukraine',
	'Russia'
];
const listCity = {
	'ukraine': [
		'Kiev', 'Kharkiv', 'Dnipro', 'Odessa', 'Donetsk', 'Zaporizhia', 'Lviv', 'Cherkasy',
		'Sumy', 'Horlivka', 'Zhytomyr', 'Kamianske', 'Kropyvnytskyi', 'Khmelnytskyi', 'Rivne'
	],
	'russia': [
		'Moscow', 'Saint', 'Novosibirsk', 'Yekaterinburg', 'Nizhny', 'Kazan', 'Chelyabinsk',
		'Omsk', 'Samara', 'Ufa', 'Krasnoyarsk', 'Perm', 'Voronezh', 'Volgograd', 'Krasnodar'
	]
};

export class NewOffice extends Component<Props, State> {
	state = {
		country: ''
	}

	changeCountry = (value: string) => {
		this.setState({country:value});
	}

	render() {
		const {pristine, submitting, handleSubmit} = this.props;
		const office = Object.assign({
			country: '',
			province: '',
			city: '',
			postal_code: '',
			street: '',
			address2: '',
			phone: '',
			fax: '',
			email: '',
			type: ''
		}, this.props.office);

		const onCancel = office.uid ? this.props.onCancel : this.props.reset;
		return (
			<form onSubmit={handleSubmit} className="new-office form-row">
				<div className="col-elem">
					<Field label="*Country:"
					       name="country"
					       initialValue={office.country}
					       classWrp="text-capitalize autocomplete"
					       type="autosuggest"
					       component={ErrorField}
					       options={listCountry}
					       onChangeValue={this.changeCountry}
					/>
					<Field label="*State/Province:"
					       name="province"
					       initialValue={office.province}
					       component={ErrorField}/>
					<Field label="*Postal Code:"
					       name="postal_code"
					       initialValue={office.postal_code}
					       component={ErrorField}/>
					<Field label="*City:"
					       name="city"
					       initialValue={office.city}
					       classWrp="text-capitalize autocomplete"
					       type="autosuggest"
					       component={ErrorField}
					       options={listCity[this.state.country.toLowerCase()]||[]}
					/>
					<Field label="*Street Address:"
					       name="street"
					       initialValue={office.street}
					       component={ErrorField}/>
					<Field label="Address 2:"
					       name="address2"
					       initialValue={office.address2}
					       component={ErrorField}/>
				</div>
				<div className="col-elem">
					<Field label="Phone:"
					       name="phone"
					       initialValue={office.phone}
					       component={ErrorField}/>
					<Field label="Fax:"
					       name="fax"
					       initialValue={office.fax}
					       component={ErrorField}/>
					<Field label="Email:"
					       name="email"
					       initialValue={office.email}
					       component={ErrorField}/>
					<Field nameField="Office Type:"
					       label="Primary HQ"
					       htmlFor={'type-office-'+Date.now()}
					       name="type"
					       initialValue={office.type}
					       type="checkbox"
					       classWrp="custom-control custom-checkbox"
					       classLabel="custom-control-label"
					       className="custom-control-input"
					       component={ErrorField}
					/>
				</div>
				<div className="col-btn">
					<button type="button" className="btn btn-outline-dark btn-sm" disabled={office.uid ? false : pristine || submitting} onClick={onCancel}>
						Cancel
					</button>
					<button type="submit" className="btn btn-primary btn-sm" disabled={pristine || submitting}>
						Save
					</button>
				</div>
			</form>
		)
	}
}

export function validate(obj: Object) {
	const {country, province, postal_code, city, street, email} = obj;
	const errors = {};
	if (!country) {
		errors.country = 'country is required';
	}
	if (!province) {
		errors.province = 'province is required';
	}
	if (!postal_code) {
		errors.postal_code = 'postal code is required';
	}
	if (!city) {
		errors.city = 'city is required';
	}
	if (!street) {
		errors.street = 'street is required';
	}
	if (email && !validateEmail.validate(email)) {
		errors.email = 'email is invalid';
	}
	return errors
}

export const NAME_FORM = 'office';

export default reduxForm({
	form: NAME_FORM,
	validate
})(NewOffice);