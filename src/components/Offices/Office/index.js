// @flow

import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import OfficeEdit from '../OfficeEdit';
import OfficeDelete from '../OfficeDelete';
import {deleteOffice, editOfficeOpen, editOfficeClose, moduleName} from '../../../ducks/office';
import {Modal, ModalBody} from 'reactstrap';
import './style.css';

type Props = {
	office: Object,
	editable: string,
	className: string,
	deleteOffice: Function,
	editOfficeClose: Function,
	editOfficeOpen: Function
};

type State = {
	remove: boolean
};

class Office extends PureComponent<Props, State> {
	state = {
		remove: false
	}

	editOfficeOpen = () => {
		const {office, editable} = this.props;
		this.props.editOfficeOpen({
			editable,
			uid: office.uid
		});
	}

	deleteOffice = () => {
		this.props.deleteOffice(this.props.office.uid);
	}

	toggleRemove = () => {
		this.setState({
			remove: !this.state.remove
		});
	}

	render() {
		const {office, className, editable, editOfficeClose} = this.props;
		
		if (editable === office.uid) {
			return <OfficeEdit office={office} onCancel={editOfficeClose} />
		}
		const {country, province, postal_code, city, street, type, address2, phone, fax, email} = office;
		const primaryHQ = type && (
			<div>
				<i className="fa fa-check" aria-hidden="true"></i>
				<span className="font-weight-bold">Primary HQ</span>
			</div>
		);
		return (
			<div className="office-wrp">
				<div className="col-data">
					<div className="d-flex">
						<div className="col-name">
							Address:
						</div>
						<div className="col-value">
							{primaryHQ}
							{street && <div>{street}</div> }
							<div>{city}{province && ', '+province}{postal_code && ' '+postal_code}</div>
							{country && <div>{country}</div> }
							{address2 && <div>{address2}</div> }
						</div>
					</div>
				</div>
				<div className="col-data">
					{phone && (
						<div className="d-flex">
							<div className="col-name">
								Phone:
							</div>
							<div className="col-value">
								{phone}
							</div>
						</div>
					)}
					{fax && (
						<div className="d-flex">
							<div className="col-name">
								Fax:
							</div>
							<div className="col-value">
								{fax}
							</div>
						</div>
					)}
					{email && (
						<div className="d-flex">
							<div className="col-name">
								Email:
							</div>
							<div className="col-value">
								{email}
							</div>
						</div>
					)}
				</div>
				<div className="col-btn">
					<button type="button" className="btn btn-outline-dark btn-sm" onClick={this.toggleRemove}>
						Remove
					</button>
					<button type="submit" className="btn btn-primary btn-sm" onClick={this.editOfficeOpen}>
						Edit
					</button>
				</div>

				<Modal isOpen={this.state.remove} toggle={this.toggleRemove} className={className} backdrop="static">
					<ModalBody>
						<OfficeDelete onSubmit={this.deleteOffice} onCancel={this.toggleRemove} />
					</ModalBody>
				</Modal>
			</div>
		);
	}
}

export default connect(state => ({
	editable: state[moduleName].editable
}), {deleteOffice, editOfficeOpen, editOfficeClose})(Office);