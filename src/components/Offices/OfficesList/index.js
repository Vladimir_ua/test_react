// @flow

import React, {Component} from 'react';
import Office from '../Office';
import './style.css';

type Props = {
	list: Object
};

class OfficesList extends Component<Props> {
	render() {
		const list = this.props.list.map(data => <Office key={data.uid} office={data.toObject()} />);
		return (
			<div className="offices-list">
				{list}
			</div>
		);
	}
}

export default OfficesList;