// @flow

import {NewOffice, validate} from '../NewOffice';
import {reduxForm} from 'redux-form';

export const NAME_FORM = 'office_edit';

export default reduxForm({
	form: NAME_FORM,
	validate
})(NewOffice);