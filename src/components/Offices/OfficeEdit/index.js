// @flow

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {editOffice} from '../../../ducks/office';
import Edit from './Edit';
import './style.css';

type Props = {
	office: Object,
	onCancel: Function,
	editOffice: Function
};

class OfficeEdit extends Component<Props> {
	onSubmit = (office) => {
		this.props.editOffice(this.props.office.uid, office);
	}

	render() {
		const {office, onCancel} = this.props;
		return (
			<Edit office={office} onSubmit={this.onSubmit} onCancel={onCancel} />
		);
	}
}

export default connect(null, {editOffice})(OfficeEdit);